<?php
class Test extends CI_Controller {

	public function _remap($first,$rest=array()){
		array_unshift($rest,$first);
		return call_user_func_array(array($this,'fun'),$rest);
	}

	public function fun($arg1,$arg2='def2'){
		...
	}
}
?>