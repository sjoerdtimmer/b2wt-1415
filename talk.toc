\beamer@endinputifotherversion {3.33pt}
\select@language {dutch}
\beamer@sectionintoc {1}{Wat je al wist}{2}{0}{1}
\beamer@sectionintoc {2}{Introducing: CondeIgniter}{12}{0}{2}
\beamer@sectionintoc {3}{Routering}{26}{0}{3}
\beamer@sectionintoc {4}{Controllers}{41}{0}{4}
\beamer@sectionintoc {5}{Views}{51}{0}{5}
\beamer@sectionintoc {6}{Models}{60}{0}{6}
\beamer@sectionintoc {7}{Libraries en Helpers}{81}{0}{7}
\beamer@sectionintoc {8}{Samenvatting}{93}{0}{8}
\beamer@sectionintoc {9}{Pagination Class}{95}{1}{9}
\beamer@sectionintoc {10}{Je eigen Library of Helper toevoegen}{99}{1}{10}
\beamer@sectionintoc {11}{Een tekortkoming van Active Record}{104}{1}{11}
\beamer@sectionintoc {12}{Speciale functies in Controllers}{109}{1}{12}
\beamer@sectionintoc {13}{Nesting}{114}{1}{13}
\beamer@sectionintoc {14}{Schrijfrechten}{121}{1}{14}
\beamer@sectionintoc {15}{Logs}{140}{1}{15}
\beamer@sectionintoc {16}{Profiling}{144}{1}{16}
