#!/bin/zsh
#monokai manni perldoc borland colorful default murphy vs trac tango fruity autumn bw emacs vim pastie friendly native
foreach style (monokai manni perldoc borland colorful default murphy vs trac tango fruity autumn bw emacs vim pastie friendly native)
	echo -n "$style..."
	pygmentize -O linenos,full,preamble='\input{extrastuff.tex}',style=$style -l html+php -o $style.tex test1.php
	pdflatex $style.tex > /dev/null
	rm $style.{aux,log}
	echo "done"
end
